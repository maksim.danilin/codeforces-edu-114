package main

import "fmt"

func GenDepth(depth int) []string {
	ret := make([]string, 0, depth)
	for i := 0; i < depth; i++ {
		var s string
		for j := 0; j < depth-i; j++ {
			s += "()"
		}
		for k := 0; k < i; k++ {
			s = "(" + s + ")"
		}
		//fmt.Println(s)
		ret = append(ret, s)
	}
	return ret
}

func main() {
	var t, n int
	var res []string
	fmt.Scanln(&t)
	for i := 0; i < t; i++ {
		fmt.Scanln(&n)
		res = append(res, GenDepth(n)...)
	}
	for _, elem := range res {
		fmt.Println(elem)
	}
}
